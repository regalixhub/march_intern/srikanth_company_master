const  AUTHORIZED_CAPResult=[0,0,0,0,0];
const fs=require('fs');
const csv=require('csv-parser');

fs.createReadStream('../csv/company_master_data_upto_Mar_2015_Maharashtra.csv')
.pipe(csv())
.on('data',(res)=>{
    if(res.AUTHORIZED_CAP < 1e5)
    AUTHORIZED_CAPResult[0]++;
    if(res.AUTHORIZED_CAP>=1e5 && res.AUTHORIZED_CAP<1e6)
    AUTHORIZED_CAPResult[1]++;
    if(res.AUTHORIZED_CAP>=1e6 && res.AUTHORIZED_CAP<1e7)
    AUTHORIZED_CAPResult[2]++;
    if(res.AUTHORIZED_CAP>=1e8 && res.AUTHORIZED_CAP<1e9)
    AUTHORIZED_CAPResult[3]++;
    if(res.AUTHORIZED_CAP>1e9)
    AUTHORIZED_CAPResult[4]++;        
})
.on('end',()=>{
    fs.writeFile('../json/AUTHORIZED_CAP_graph.json',JSON.stringify(AUTHORIZED_CAPResult),() => {});
});

