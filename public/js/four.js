const fs=require('fs');
const csv=require('csv-parser');
const PBARegistrations2015={};
const PBARegistrationsPerYear={};

    
fs.createReadStream('../csv/company_master_data_upto_Mar_2015_Maharashtra.csv')
.pipe(csv())
.on('data',(data)=>{
  let y;
    
  const date = data.DATE_OF_REGISTRATION.split('-');
    if (date[2] >= '2010' && date[2] <= '2018') {
      if (!PBARegistrationsPerYear[data.PRINCIPAL_BUSINESS_ACTIVITY]) {
        PBARegistrationsPerYear[data.PRINCIPAL_BUSINESS_ACTIVITY] = {};
        y = PBARegistrationsPerYear[data.PRINCIPAL_BUSINESS_ACTIVITY];
        if (!y[date[2]]) {
          y[date[2]] = 1;
        }
      } else {
        y = PBARegistrationsPerYear[data.PRINCIPAL_BUSINESS_ACTIVITY];
        if (!y[date[2]]) {
          y[date[2]] = 1;
        } else {
          y[date[2]] += 1;
        }
      }
    }
  })
.on('end',()=>{
    fs.writeFile('../json/Year_Activity.json',JSON.stringify(PBARegistrationsPerYear),() => {});
});

    







