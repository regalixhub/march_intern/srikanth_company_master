const fs=require('fs');
const csv=require('csv-parser');
const PBARegistrations2015={};
fs.createReadStream('..csv/company_master_data_upto_Mar_2015_Maharashtra.csv')
.pipe(csv())
.on('data',(res)=>{
    const date = res.DATE_OF_REGISTRATION.split('-');
    if (date[2] === '2015') {
        if (!PBARegistrations2015[res.PRINCIPAL_BUSINESS_ACTIVITY]) {
          PBARegistrations2015[res.PRINCIPAL_BUSINESS_ACTIVITY] = 1;
        } else {
          PBARegistrations2015[res.PRINCIPAL_BUSINESS_ACTIVITY] += 1;
        }
      }
  
})
.on('end',()=>{
    console.log(PBARegistrations2015);
    fs.writeFile('..json/PBARegistrations_2015.json',JSON.stringify(PBARegistrations2015),() => {});
});

    
