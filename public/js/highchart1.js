fetch('../json/AUTHORIZED_CAP_graph.json')
    .then(function(response) {
        response.json().then(function(data) {
          
    Highcharts.chart('chart1', {
      chart: {
        type: 'column',
      },
      title: {
        text: 'AUTHORIZED CAPITAL',
      },
      xAxis: {
        categories: ['capitalLessThan1L', 'capital1LTo10L', 'capital10LTo1Cr', 'capital10CrTo100Cr', 'capitalMoreThan100Cr'],
      },
      yAxis: {
        title: {
          text: 'AUTHORIZED CAPITAL INVESTED ',
        },
      },
      plotOptions: {
        column: {
          dataLabels: {
            enabled: true,
          },
        },
      },
      series: [{
        name: 'AUTHORIZED CAPITAL',
        data: data,
       }],
    });
  })
})
